import React from 'react';
import logo from './logo.svg';

function App() {
  return (
    <div className="App">
      My awesome game hub
    </div>
  );
}

export default App;
